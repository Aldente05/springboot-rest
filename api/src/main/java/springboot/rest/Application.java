package springboot.rest;

import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * Created by f.putra on 9/29/17.
 */

/**
 * @SpringBootApplication ini adalah annotasi main class untuk spring boot
 * annotasi ini equivalent dengan @EnableAutoConfiguration, @ComponentScan and @Configuration
 */
@SpringBootApplication()
@ControllerAdvice(basePackages = "springboot.rest.controller")
@EnableAutoConfiguration(exclude = {
        RepositoryRestMvcAutoConfiguration.class
})
public class Application implements CommandLineRunner {

    /**
     * ini adalah main method untuk menjalankan spring boot
     * @param args
     */
    public static void main(String... args) {
        new SpringApplicationBuilder().bannerMode(Banner.Mode.OFF).run(args);
    }

    @Override
    public void run(String... args) throws Exception {}
}
