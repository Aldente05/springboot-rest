package springboot.rest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by f.putra on 9/30/17.
 */

/**
 * @RestController adalah annotasi pada Spring 4
 * yang pada dahulu kala memakai @Controller dan @ResponseBody
 * sekarang hanya memakai @RestController
 */
@RestController
public class SampleController {

    /**
     * @GetMapping berarti ini request get
     * @PostMapping berarti request post
     * @PutMapping berarti request put
     * @DeleteMapping berarti request delete
     * <p>
     * ketika membuat crud kita memakai 4 annotasi untuk mapping (POST, GET, UPDATE, DELETE)
     * update bisa memakai post annotasi
     */

    @GetMapping(value = "/")
    public String sample() {
        return "HELLO";
    }
}
