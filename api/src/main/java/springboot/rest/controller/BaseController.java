package springboot.rest.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by f.putra on 9/30/17.
 */
public class BaseController {

    protected Locale ID = new Locale("id");

    /**
     * Converting springboot.rest.core.entity object to json value with specific format
     *
     * @param data
     * @param status
     * @return
     */
    public Map<String, Object> getCallBack(Object data, Object status) {
        Map<String, Object> model = new HashMap<>();
        model.put("data", data);
        model.put("status", status);
        return model;
    }

    public Map<String, Object> getCallBack(Object data, Object rows, Object status) {
        Map<String, Object> model = new HashMap<>();
        model.put("data", data);
        model.put("rows", rows);
        model.put("status", status);
        return model;
    }

    /**
     * @param data
     * @return
     */
    public Map<String, Object> getCallBack(Object data) {
        Map<String, Object> model = new HashMap<>();
        model.put("data", data);
        return model;
    }

    /**
     * Converting springboot.rest.core.entity object to json value with specific format
     *
     * @param status
     * @return
     */
    public Map<String, Object> getCallBack(String status) {
        Map<String, Object> model = new HashMap<>();
        model.put("status", status);
        return model;
    }
}
