package springboot.test.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import springboot.rest.core.entity.User;
import springboot.rest.core.service.UserService;

/**
 * Created by f.putra on 03/04/18.
 */
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Test
    public void saveTest(){
        User user = new User();
        user.setName("Test");
        user.setPassword("passwordTest");

        userService.save(user);

    }
}
