package springboot.rest.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by f.putra on 02/08/18.
 */
@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = -3187404599343778325L;

    private String name;
    private String password;
}
