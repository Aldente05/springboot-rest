package springboot.rest.core;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by f.putra on 9/29/17.
 */
@SpringBootApplication
public class AppCore {
}
