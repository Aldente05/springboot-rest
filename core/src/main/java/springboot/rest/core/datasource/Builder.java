package springboot.rest.core.datasource;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by f.putra on 9/29/17.
 */
@Configuration
@ComponentScan(basePackages = {"springboot.rest.core.service",
        "springboot.rest.core.common",
        "springboot.rest.core.aspect"})
@EnableJpaRepositories(basePackages = {"springboot.rest.core.repository"})
@EnableTransactionManagement
public class Builder {
}
