package springboot.rest.core.common;


/**
 * @author f.putra
 */
public class Pattern {

    public static final String YYYY_DD_MM_WITH_STRIP = "yyyy-dd-mm";
    public static final String DD_MM_YYYY_WITH_STRIP = "dd-mm-yyyy";
    public static final String YY_MM_DD_T_TIME_WITH_STRIP = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String YYYY_MM_DD_WITH_SLASH = "yyyy/MM/dd";
    public static final String YYYY_MM_DD_TIME_WITH_SLASH = "yyyy/MM/dd HH:mm:ss";
    public static final String DD_MM_YYYY_WITH_BACK_SLASH = "dd/MM/yyyy";
    public static final String MM_YYYY_WITH_SLASH = "MM/yyyy";
    public static final String DATE_TIME_PATTERN = "dd-MM-yyyy hh:mm:ss";
    public static final String DATE_WITH_ZONE_DETAIL = "EEE MMM dd HH:mm:ss z yyyy";
    public static final String MMMM_DD_YYYY = "MMMM, dd yyyy";
    public static final String yyyy_MM_ddTHH_mm_ss_SSSZ="yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String MMMM_YYYY = "MMMM yyyy";
}
