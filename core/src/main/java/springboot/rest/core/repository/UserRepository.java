package springboot.rest.core.repository;

import springboot.rest.core.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by f.putra on 02/08/18.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
