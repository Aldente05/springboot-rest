package springboot.rest.core.service;

import springboot.rest.core.entity.User;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by f.putra on 02/08/18.
 */
public class UserService implements BaseService<User>{

    private EntityManager entityManager;


    @Override
    public User save(User entity) {
        return null;
    }

    @Override
    public User update(User entity) {
        return null;
    }

    @Override
    public User delete(User entity) {
        return null;
    }

    @Override
    public User findById(Long id) {
        return null;
    }

    @Override
    public List<User> find(User param, Pageable pageable) {
        return null;
    }

    @Override
    public int count(User param) {
        return 0;
    }
}
