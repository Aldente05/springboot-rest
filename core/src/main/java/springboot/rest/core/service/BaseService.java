package springboot.rest.core.service;

import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by f.putra on 03/01/18.
 * base springboot.rest.core.service yang biasa digunakan
 */
public interface BaseService<T>{

    T save(final T entity);

    T update(final T entity);

    T delete(final T entity);

    T findById(final Long id);

    /**
     * Get all data with and without any parameter
     */
    List<T> find(T param, Pageable pageable);

    /**
     * Count all data with and without any parameter
     * <p>You can include the parameter using ${@link T} class
     *
     * @return
     */
    int count(T param);
}
